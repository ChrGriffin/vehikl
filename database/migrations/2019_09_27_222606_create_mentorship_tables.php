<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMentorshipTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mentorship_users', function (Blueprint $table) {
            $table->string('id')->nullable(false);
            $table->string('name')->nullable(false);
            $table->timestamps();

            $table->primary('id');
        });

        Schema::create('mentorship_skills', function (Blueprint $table) {
            $table->string('name')->nullable(false);
            $table->timestamps();

            $table->primary('name');
        });

        Schema::create('mentorship_users_x_mentorship_skills', function (Blueprint $table) {
            $table->string('user_id')->nullable(false);
            $table->string('skill_name')->nullable(false);
            $table->enum('type', ['mentor', 'mentee']);

            $table->primary(['user_id', 'skill_name']);
            $table->foreign('user_id')->references('id')->on('mentorship_users')->onDelete('CASCADE');
            $table->foreign('skill_name')->references('name')->on('mentorship_skills')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mentorship_users_x_mentorship_skills');
        Schema::dropIfExists('mentorship_skills');
        Schema::dropIfExists('mentorship_users');
    }
}
