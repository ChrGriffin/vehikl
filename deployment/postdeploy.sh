#!/bin/sh

# REMEMBER: pass a deployment environment to this script as an argument! ie, `source postdeploy.sh STAGE`
# REMEMBER: pass a the password for the deployment user to this script as an argument~ ie, `source postdeploy.sh STAGE password`

# Remove any setup files.
rm app.zip

# If we are deploying to the staging environment
if [ $1 = "STAGE" ]; then

    # Run a fresh migration
    # This will delete all tables and run every migration fresh
    php artisan migrate:fresh --force

    # Seed the new tables with data
    php artisan db:seed --force

# If we are deploying to the live environment
elif [ $1 = "PROD" ]; then

    # Run Laravel migrations on the database.
    # By adding the --force flag, we prevent any blocking dialogue.
    php artisan migrate --force
fi

# Restart application workers
#echo $2 | sudo -S supervisorctl restart cg-worker:*
