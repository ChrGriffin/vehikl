<?php

Route::group(['prefix' => 'parroter'], function () {
    Route::get('/authenticate', 'ParroterController@authenticate')->name('parroter.oauth');
    Route::get('/oauth/reaction', 'ParroterController@oauthCallbackReactions')->name('parroter.oauth.callback.reaction');
    Route::get('/oauth/final', 'ParroterController@oauthCallbackFinal')->name('parroter.oauth.callback.final');
});

Route::group(['prefix' => 'veetup'], function () {
    Route::get('/authenticate', 'VeetupController@authenticate')->name('veetup.oauth');
    Route::get('/oauth', 'VeetupController@oauthCallback')->name('veetup.oauth.callback');
});
