<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'nexmo' => [
        'key' => env('NEXMO_KEY'),
        'secret' => env('NEXMO_SECRET')
    ],

    'slack' => [
        'parroter' => [
            'oauth_token' => env('PARROTER_SLACK_OAUTH_KEY'),
            'client_id' => env('PARROTER_SLACK_KEY'),
            'client_secret' => env('PARROTER_SLACK_SECRET'),
            'redirect_reaction' => env('PARROTER_SLACK_REDIRECT_URI_REACTION'),
            'redirect_final' => env('PARROTER_SLACK_REDIRECT_URI_FINAL')
        ],
        'veetup' => [
            'webhook' => env('VEETUP_SLACK_WEBHOOK')
        ],
        'mentorship' => [
            'oauth_token' => env('MENTORSHIP_SLACK_OAUTH_KEY'),
        ],
        'decarlo_bot' => [
            'channel_name' => env('DECARLO_SLACK_CHANNEL_NAME'),
            'oauth_token' => env('DECARLO_SLACK_OAUTH_KEY')
        ],
        'galway_bot' => [
            'channel_name' => env('GALWAY_SLACK_CHANNEL_NAME'),
            'oauth_token' => env('GALWAY_SLACK_OAUTH_KEY')
        ]
    ],

    'meetup' => [
        'veetup' => [
            'client_id' => env('VEETUP_MEETUP_KEY'),
            'client_secret' => env('VEETUP_MEETUP_SECRET'),
            'redirect' => env('VEETUP_MEETING_REDIRECT_URI'),
        ]
    ],

    'words_api' => [
        'decarlo_bot' => [
            'key' => env('DECARLO_WORD_API_KEY')
        ]
    ],

    'quotes_api' => [
        'decarlo_bot' => [
            'key' => env('DECARLO_QUOTE_API_KEY')
        ]
    ]

];
