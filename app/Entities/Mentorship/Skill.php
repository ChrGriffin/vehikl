<?php

namespace App\Entities\Mentorship;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Skill extends Model
{
    /**
     * @var string
     */
    protected $table = 'mentorship_skills';

    /**
     * @var string
     */
    protected $primaryKey = 'name';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * @return BelongsToMany
     */
    public function mentors(): BelongsToMany
    {
        return $this->users()->wherePivot('type', '=', 'mentor');
    }

    /**
     * @return BelongsToMany
     */
    public function mentees(): BelongsToMany
    {
        return $this->users()->wherePivot('type', '=', 'mentee');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            'mentorship_users_x_mentorship_skills',
            'skill_name',
            'user_id'
        );
    }

}
