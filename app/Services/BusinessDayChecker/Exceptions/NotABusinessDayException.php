<?php

namespace App\Services\BusinessDayChecker;

use Carbon\Carbon;
use Throwable;

class NotABusinessDayException extends \InvalidArgumentException
{
    public function __construct(Carbon $day, $code = 0, Throwable $previous = null)
    {
        parent::__construct($day->toDateString() . ' is not a business day', $code, $previous);
    }
}
