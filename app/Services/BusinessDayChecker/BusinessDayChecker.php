<?php

namespace App\Services\BusinessDayChecker;

use App\Services\HolidaysApi\Holiday;
use App\Services\HolidaysApi\HolidaysApi;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class BusinessDayChecker
{
    /** @var HolidaysApi */
    private $holidaysApi;

    /** @var Collection */
    private $holidays;

    /** @var Collection */
    private $businessDays;

    public function __construct(HolidaysApi $holidaysApi)
    {
        $this->holidaysApi = $holidaysApi;
        $this->holidays = $this->holidaysApi->holidays();
        $this->businessDays = $this->calculateBusinessDays();
    }

    private function calculateBusinessDays(): Collection
    {
        $days = collect();

        $day = now()->startOfYear()->startOfDay();
        $endOfYear = now()->endOfYear()->startOfDay();
        while($day->lessThanOrEqualTo($endOfYear)) {
            if(!$day->isWeekend() && !$this->dayIsHoliday($day)) {
                $days->push($day->copy());
            }
            $day->addDay();
        }

        return $days;
    }

    private function dayIsHoliday(Carbon $day): bool
    {
        return $this->holidays->contains(function (Holiday $holiday) use ($day) {
            return $holiday->getDate()->isSameDay($day);
        });
    }

    public function dayIsBusinessDay(Carbon $day): bool
    {
        return $this->businessDays->contains(function (Carbon $businessDay) use ($day) {
            return $businessDay->isSameDay($day);
        });
    }

    public function numberForBusinessDay(Carbon $day): int
    {
        $index = $this->businessDays->search(function (Carbon $businessDay) use ($day) {
            return $businessDay->isSameDay($day);
        });

        if($index === false) {
            throw new NotABusinessDayException($day);
        }

        return $index + 1;
    }
}
