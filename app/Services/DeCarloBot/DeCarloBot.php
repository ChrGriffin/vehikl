<?php

namespace App\Services\DeCarloBot;

use App\Services\DeCarloBot\Dailies\DailyInterface;
use App\Services\DeCarloBot\Dailies\Quote;
use App\Services\DeCarloBot\Dailies\TodayInHistory;
use App\Services\DeCarloBot\Dailies\Word;
use App\Services\Slack\Slack;

class DeCarloBot
{
    /** @var Slack */
    private $slack;

    /** @var string[] */
    private $dailies = [Word::class, Quote::class, TodayInHistory::class];

    public function __construct(Slack $slack)
    {
        $this->slack = $slack;
    }

    public function postDaily(?string $dailyClass = null)
    {
        /** @var DailyInterface $daily */
        $daily = app()->make(is_null($dailyClass) ? $this->dailies[array_rand($this->dailies)] : $dailyClass);
        $this->slack->postMessage(
            config('services.slack.decarlo_bot.channel_name'),
            $daily->generate()->toSlackMessage()
        );
    }
}
