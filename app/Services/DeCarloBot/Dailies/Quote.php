<?php

namespace App\Services\DeCarloBot\Dailies;

use App\Services\DeCarloBot\Exceptions\DailyNotGeneratedException;
use App\Services\QuotesApi\QuotesApi;

class Quote implements DailyInterface
{
    /** @var QuotesApi */
    private $api;

    /**
     * @var \App\Services\QuotesApi\Quote
     */
    private $quote;

    public function __construct(QuotesApi $api)
    {
        $this->api = $api;
    }

    public function generate(): DailyInterface
    {
        $this->quote = $this->api->random();
        return $this;
    }

    public function toSlackMessage(): string
    {
        if(is_null($this->quote)) {
            throw new DailyNotGeneratedException();
        }

        return "Today's quote of the day is:\n*" . $this->quote->getQuote() . "*\n-_" . $this->quote->getAuthor() . '_';
    }
}
