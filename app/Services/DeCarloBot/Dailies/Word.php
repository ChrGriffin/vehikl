<?php

namespace App\Services\DeCarloBot\Dailies;

use App\Services\DeCarloBot\Exceptions\DailyNotGeneratedException;
use App\Services\WordsApi\WordsApi;

class Word implements DailyInterface
{
    /** @var WordsApi */
    private $api;

    /**
     * @var \App\Services\WordsApi\Word
     */
    private $word;

    public function __construct(WordsApi $api)
    {
        $this->api = $api;
    }

    public function generate(): DailyInterface
    {
        $this->word = $this->api->randomWithDefinition();
        return $this;
    }

    public function toSlackMessage(): string
    {
        if(is_null($this->word)) {
            throw new DailyNotGeneratedException();
        }

        $message = 'Today\'s word of the day is *' . $this->word->getWord() . '*.';

        if(!is_null($this->word->getType()) && !is_null($this->word->getDefinition())) {
            $message .= ' It is a ' . $this->word->getType() . ' that is defined as *' . $this->word->getDefinition() . '*.';
        }
        else {
            if(!is_null($this->word->getDefinition())) {
                $message .= ' It is defined as *' . $this->word->getDefinition() . '*.';
            }

            if(!is_null($this->word->getType())) {
                $message .= ' It is a ' . $this->word->getType() . '.';
            }
        }

        if(!is_null($this->word->getPronunciation())) {
            $message .= ' It is pronounced _' . $this->word->getPronunciation() . '_.';
        }

        return $message;
    }
}
