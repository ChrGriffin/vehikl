<?php

namespace App\Services\DeCarloBot\Dailies;

use App\Services\DeCarloBot\Exceptions\DailyNotGeneratedException;
use App\Services\TodayInHistoryApi\Event;
use App\Services\TodayInHistoryApi\TodayInHistoryApi;

class TodayInHistory implements DailyInterface
{
    /** @var TodayInHistoryApi */
    private $api;

    /**
     * @var Event
     */
    private $event;

    public function __construct(TodayInHistoryApi $api)
    {
        $this->api = $api;
    }

    public function generate(): DailyInterface
    {
        $this->event = $this->api->random();
        return $this;
    }

    public function toSlackMessage(): string
    {
        if(is_null($this->event)) {
            throw new DailyNotGeneratedException();
        }

        return 'Today in history, in the year *' . $this->event->getYear() . "*:\n*" . $this->event->getText() . '*';
    }
}
