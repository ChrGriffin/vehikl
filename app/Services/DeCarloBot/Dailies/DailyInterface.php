<?php

namespace App\Services\DeCarloBot\Dailies;

interface DailyInterface
{
    public function generate(): DailyInterface;

    public function toSlackMessage(): string;
}
