<?php

namespace App\Services\DeCarloBot\Exceptions;

use Throwable;

class DailyNotGeneratedException extends \Exception
{
    public function __construct(
        $message = 'Dailies must be generated via generate() before they can be transformed.',
        $code = 0,
        Throwable $previous = null
    )
    {
        parent::__construct($message, $code, $previous);
    }
}
