<?php

namespace App\Services\TodayInHistoryApi;

class Event
{
    /** @var string */
    private $text;

    /** @var int */
    private $year;

    public function __construct(\stdClass $event)
    {
        $this->text = $event->text;
        $this->year = (int) $event->year;
    }

    public function toArray(): array
    {
        return [
            'text' => $this->getText(),
            'year' => $this->getYear()
        ];
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getYear(): int
    {
        return $this->year;
    }
}
