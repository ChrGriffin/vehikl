<?php

namespace App\Services\TodayInHistoryApi;

use GuzzleHttp\Client;

class TodayInHistoryApi
{
    /** @const string */
    const ENDPOINT = 'https://history.muffinlabs.com/date';

    /** @var Client */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function random(): Event
    {
        $response = $this->client->get(self::ENDPOINT);
        $events = json_decode($response->getBody()->getContents())->data->Events;
        return new Event($events[array_rand($events)]);

    }
}
