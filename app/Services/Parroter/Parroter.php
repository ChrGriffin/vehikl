<?php

namespace App\Services\Parroter;

use App\Services\Slack\Slack;
use App\SlackApp;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Mpociot\Socialite\Slack\Provider;
use Mpociot\Socialite\Slack\Provider as SlackProvider;
use SocialiteProviders\Manager\Config as SocialiteConfig;

class Parroter
{
    /**
     * @var SlackApp
     */
    protected $slackApp;

    /**
     * @var GuzzleClient;
     */
    protected $guzzle;

    /**
     * @var Slack
     */
    private $slack;

    /**
     * @param Slack $slack
     * @return void
     */
    public function __construct(Slack $slack)
    {
        $this->slackApp = SlackApp::where('name', 'parroter')->firstOrFail();
        $this->guzzle = resolve('GuzzleHttp\Client');
        $this->slack = $slack;
    }

    /**
     * @return mixed
     */
    public function oauthRedirectToReaction()
    {
        $this->configureOauth('reaction');
        return Socialite::with('slack')->scopes([
            'identity.basic'
        ])->redirect();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function oauthRedirectToFinal(Request $request)
    {
        $this->configureOauth('reaction');
        $token = Socialite::driver('slack')->stateless()->getAccessToken($request->input('code'));

        $this->configureOauth('final');
        return Socialite::with('slack')
            ->scopes(['reactions:write'])
            ->with(['token' => $token])
            ->redirect();
    }

    /**
     * @param Request $request
     * @return void
     */
    public function saveUserFromRequest(Request $request): void
    {
        $this->configureOauth('final');

        /** @var Provider $slack */
        $slack = Socialite::driver('slack')->stateless();
        $token = $slack->getAccessToken($request->input('code'));
        $user = json_decode(
            $this->guzzle->get('https://slack.com/api/users.identity?token='.$token)->getBody()->getContents()
        )->user;
        $this->saveUserToken($user->id, $token);
    }

    /**
     * @param string $endpoint
     * @return void
     */
    private function configureOauth(string $endpoint): void
    {
        config(['services.slack.client_id' => config('services.slack.parroter.client_id')]);
        config(['services.slack.client_secret' => config('services.slack.parroter.client_secret')]);
        config(['services.slack.redirect' => config("services.slack.parroter.redirect_$endpoint")]);

        /** @var SlackProvider $slack */
        $slack = Socialite::driver('slack')->stateless();
        $slack->setConfig($this->createSocialiteConfig($endpoint));
    }

    /**
     * @param string $endpoint
     * @return SocialiteConfig
     */
    private function createSocialiteConfig(string $endpoint): SocialiteConfig
    {
        return new SocialiteConfig(
            config('services.slack.parroter.client_id'),
            config('services.slack.parroter.client_secret'),
            config('services.slack.parroter.redirect_' . $endpoint)
        );
    }

    /**
     * @param string $userId
     * @param string $token
     * @return bool
     */
    public function saveUserToken(string $userId, string $token) : bool
    {
        $appData = $this->slackApp->data;
        if(!isset($appData['tokens'])) {
            $appData['tokens'] = [];
        }

        $appData['tokens'][$userId] = $token;
        $this->slackApp->data = $appData;
        return $this->slackApp->save();
    }

    /**
     * @param string $userId
     * @return bool
     */
    public function userTokenExists(string $userId) : bool
    {
        return !empty($this->slackApp->data['tokens'][$userId]);
    }

    /**
     * @param string $userId
     * @return null|string
     */
    public function getUserToken(string $userId) : ?string
    {
        return $this->slackApp->data['tokens'][$userId] ?? null;
    }

    /**
     * @param string $channelId
     * @param string $userId
     * @return bool
     */
    public function requestAuthentication(string $channelId, string $userId) : bool
    {
        $this->slack->postEphemeral(
            $userId,
            $channelId,
            'You must authenticate with Parroter before you can use to post reactions.',
            [
                [
                    'title' => 'Authentication Required',
                    'fallback' => 'Sign in here: ' . route('parroter.oauth'),
                    'actions' => [
                        [
                            'type' => 'button',
                            'text' => 'Sign In Here',
                            'url'  => route('parroter.oauth')
                        ]
                    ]
                ]
            ]
        );

        return true;
    }

    /**
     * @return array
     */
    public function getAllParrots() : array
    {
        return array_keys(array_filter(
            (array)json_decode($this->slack->listEmojis()->getBody()->getContents(), true)['emoji'],
            function ($emojiName) {
                return strpos($emojiName, 'parrot') > -1;
            },
            ARRAY_FILTER_USE_KEY
        ));
    }

    /**
     * @param string $token
     * @param string $channel
     * @param string $timestamp
     * @return bool
     */
    public function postReactions(string $token, string $channel, string $timestamp) : bool
    {
        foreach($this->getAllParrots() as $emoji) {
            $this->slack->postReaction($channel, $timestamp, $emoji, $token);
        }

        return true;
    }
}
