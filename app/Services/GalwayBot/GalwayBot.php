<?php

namespace App\Services\GalwayBot;

use App\Services\BusinessDayChecker\BusinessDayChecker;
use App\Services\Slack\Slack;
use Carbon\Carbon;
use NumberFormatter;

class GalwayBot
{
    /** @var Slack */
    private $slack;

    /** @var BusinessDayChecker */
    private $businessDayChecker;

    /** @var array */
    private $messages = [
        'Let\'s push it!',
        'Push hard today!',
        'We got this!',
        'Push it to the limit!',
        'Live to win!',
        'We can do it!',
        'Success is a choice!',
        'What is humanity but a random experiment in cruelty? What is life but a parade to the grave? Eat at Arby\'s.',
        'Your limitation is only your imagination!',
        'Let\'s push ourselves today!',
        'I believe in us!',
        'We have the power!'
    ];

    public function __construct(Slack $slack, BusinessDayChecker $businessDayChecker)
    {
        $this->slack = $slack;
        $this->businessDayChecker = $businessDayChecker;
    }

    public function postIfTodayIsBusinessDay(): void
    {
        $today = now();
        if($this->businessDayChecker->dayIsBusinessDay($today)) {
            $this->slack->postMessage(
                config('services.slack.galway_bot.channel_name'),
                $this->createMessage($today)
            );
        }
    }

    private function createMessage(Carbon $date): string
    {
        $ordinalNumber = (new NumberFormatter('en_US', NumberFormatter::ORDINAL))
            ->format($this->businessDayChecker->numberForBusinessDay($date));

        return 'Today is the '
            . $ordinalNumber
            . ' business day of the year. *'
            . $this->messages[array_rand($this->messages)]
            . '*';
    }
}
