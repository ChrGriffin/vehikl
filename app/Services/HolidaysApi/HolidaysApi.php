<?php

namespace App\Services\HolidaysApi;

use GuzzleHttp\Client;
use Illuminate\Support\Collection;

class HolidaysApi
{
    /** @const string */
    const ENDPOINT = 'https://canada-holidays.ca/api/v1/provinces/ON';

    /** @var Client */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function holidays(): Collection
    {
        $response = $this->client->get(self::ENDPOINT);
        $holidays = json_decode($response->getBody()->getContents())->province->holidays;

        return collect($holidays)
            ->map(function (\stdClass $holiday) {
                return new Holiday($holiday);
            });
    }
}
