<?php

namespace App\Services\HolidaysApi;

use Carbon\Carbon;

class Holiday
{
    /** @var Carbon */
    private $date;

    /** @var string */
    private $name;

    public function __construct(\stdClass $holiday)
    {
        $this->date = Carbon::parse($holiday->date);
        $this->name = $holiday->nameEn;
    }

    public function toArray(): array
    {
        return [
            'date' => $this->getDate()->format('Y-m-d'),
            'name' => $this->getName()
        ];
    }

    public function getDate(): Carbon
    {
        return $this->date;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
