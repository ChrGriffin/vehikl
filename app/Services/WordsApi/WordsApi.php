<?php

namespace App\Services\WordsApi;

use GuzzleHttp\Client;

class WordsApi
{
    /** @const string */
    const ENDPOINT = 'https://wordsapiv1.p.rapidapi.com';

    /** @var Client */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function random(): Word
    {
        do {
            $response = $this->client->get(self::ENDPOINT . '/words/', [
                'query' => [
                    'random' => 'true'
                ],
                'headers' => [
                    'x-rapidapi-host' => 'wordsapiv1.p.rapidapi.com',
                    'x-rapidapi-key' => config('services.words_api.decarlo_bot.key')
                ]
            ]);

            $word = new Word(json_decode($response->getBody()->getContents()));
        }
        while(str_contains($word->getWord(), ' '));

        return $word;
    }

    public function randomWithDefinition(): Word
    {
        do {
            $word = $this->random();
        }
        while(is_null($word->getDefinition()));

        return $word;
    }
}
