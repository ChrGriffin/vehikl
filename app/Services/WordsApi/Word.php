<?php

namespace App\Services\WordsApi;

class Word
{
    /** @var string */
    private $word;

    /** @var null|string */
    private $definition;

    /** @var null|string */
    private $type;

    /** @var null|string */
    private $pronunciation;

    public function __construct(\stdClass $word)
    {
        $this->word = $word->word;
        $this->definition = isset($word->results[0]->definition) ? $word->results[0]->definition : null;
        $this->type = isset($word->results[0]->partOfSpeech) ? $word->results[0]->partOfSpeech : null;
        $this->pronunciation = isset($word->pronunciation->all) ? $word->pronunciation->all : null;
    }

    public function toArray(): array
    {
        return [
            'word'          => $this->getWord(),
            'definition'    => $this->getDefinition(),
            'type'          => $this->getType(),
            'pronunciation' => $this->getPronunciation()
        ];
    }

    public function getWord(): string
    {
        return $this->word;
    }

    public function getDefinition(): ?string
    {
        return $this->definition;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function getPronunciation(): ?string
    {
        return $this->pronunciation;
    }
}
