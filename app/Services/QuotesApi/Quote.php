<?php

namespace App\Services\QuotesApi;

class Quote
{
    /** @var string */
    private $quote;

    /** @var string */
    private $author;

    public function __construct(\stdClass $quote)
    {
        $this->quote = $quote->quote;
        $this->author = $quote->author;
    }

    public function toArray(): array
    {
        return [
            'quote' => $this->getQuote(),
            'author' => $this->getAuthor()
        ];
    }

    public function getQuote(): string
    {
        return $this->quote;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }
}
