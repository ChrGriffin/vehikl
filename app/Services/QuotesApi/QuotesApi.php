<?php

namespace App\Services\QuotesApi;

use GuzzleHttp\Client;

class QuotesApi
{
    /** @const string */
    const ENDPOINT = 'https://andruxnet-random-famous-quotes.p.rapidapi.com';

    /** @var Client */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function random(): Quote
    {
        $response = $this->client->get(self::ENDPOINT, [
            'query'   => [
                'famous' => 'true'
            ],
            'headers' => [
                'x-rapidapi-host' => 'andruxnet-random-famous-quotes.p.rapidapi.com',
                'x-rapidapi-key'  => config('services.quotes_api.decarlo_bot.key')
            ]
        ]);

        return new Quote(json_decode($response->getBody()->getContents())[0]);
    }
}
