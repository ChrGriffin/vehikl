<?php

namespace App\Providers;

use App\Services\BusinessDayChecker\BusinessDayChecker;
use App\Services\DeCarloBot\DeCarloBot;
use App\Services\GalwayBot\GalwayBot;
use App\Services\HolidaysApi\HolidaysApi;
use App\Services\IsFelipeAliveService;
use App\Services\Mentorship\Mentorship;
use App\Services\Parroter\Parroter;
use App\Services\Slack\Slack;
use App\Services\VeetupService;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Nexmo\Client as NexmoClient;
use Nexmo\Client\Credentials\Basic as NexmoCredentialsBasic;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    public function register()
    {
        $this->bindIsFelipeAlive();
        $this->bindParroter();
        $this->bindVeetup();
        $this->bindMentorship();
        $this->bindDeCarloBot();
        $this->bindGalwayBot();

        $this->app->singleton('Nexmo\Client', function () {
            return new NexmoClient(new NexmoCredentialsBasic(
                config('services.nexmo.key'),
                config('services.nexmo.secret')
            ));
        });

        $this->app->bind('GuzzleHttp\Client', function () {
            return new Client;
        });

        $this->app->bind(Slack::class, function () {
            return new Slack($this->app->make(Client::class), config('services.slack.parroter.oauth_token'));
        });
    }

    private function bindIsFelipeAlive(): void
    {
        $this->app->singleton('App\Services\IsFelipeAliveService', function () {
            return new IsFelipeAliveService;
        });
    }

    private function bindParroter(): void
    {
        $this->app->singleton(Parroter::class, function () {
            return new Parroter($this->app->make(Slack::class));
        });
    }

    private function bindVeetup(): void
    {
        $this->app->singleton('App\Services\VeetupService', function () {
            return new VeetupService;
        });
    }

    private function bindMentorship(): void
    {
        $this->app->when(Mentorship::class)
            ->needs(Slack::class)
            ->give(function () {
                return new Slack($this->app->make(Client::class), config('services.slack.mentorship.oauth_token'));
            });
    }

    private function bindDeCarloBot(): void
    {
        $this->app->when(DeCarloBot::class)
            ->needs(Slack::class)
            ->give(function () {
                return new Slack($this->app->make(Client::class), config('services.slack.decarlo_bot.oauth_token'));
            });
    }

    private function bindGalwayBot(): void
    {
        $this->app->when(GalwayBot::class)
            ->needs(Slack::class)
            ->give(function () {
                return new Slack($this->app->make(Client::class), config('services.slack.galway_bot.oauth_token'));
            });
    }
}
