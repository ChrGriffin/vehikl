<?php

namespace App\Console\Commands;

use App\Services\GalwayBot\GalwayBot;
use Illuminate\Console\Command;

class GalwayBotPostBusinessDay extends Command
{
    /** @var string */
    protected $signature = 'galway:business-day';

    /** @var string */
    protected $description = 'If it is a business day, post the current business day to the Ableto channel.';

    public function handle(GalwayBot $galwayBot)
    {
        $galwayBot->postIfTodayIsBusinessDay();
    }
}
