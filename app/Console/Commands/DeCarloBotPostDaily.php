<?php

namespace App\Console\Commands;

use App\Services\DeCarloBot\DeCarloBot;
use Illuminate\Console\Command;

class DeCarloBotPostDaily extends Command
{
    /** @var string */
    protected $signature = 'decarlo:daily {--daily=}';

    /** @var string */
    protected $description = 'Post a random \'X of the day\' to Slack.';

    public function handle()
    {
        $deCarloBot = resolve(DeCarloBot::class);

        if(!is_null($this->option('daily'))) {
            $deCarloBot->postDaily('\\App\\Services\\DeCarloBot\\Dailies\\' . $this->option('daily'));
            return;
        }

        $deCarloBot->postDaily();
    }
}
