<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Services\Parroter\Parroter;

class ParroterController extends Controller
{
    /**
     * @param Parroter $parroter
     * @return RedirectResponse
     */
    public function authenticate(Parroter $parroter)
    {
        return $parroter->oauthRedirectToReaction();
    }

    /**
     * @param Parroter $parroter
     * @param Request $request
     * @return View
     */
    public function oauthCallbackReactions(Parroter $parroter, Request $request)
    {
        return $parroter->oauthRedirectToFinal($request);
    }

    /**
     * @param Parroter $parroter
     * @param Request $request
     * @return View
     */
    public function oauthCallbackFinal(Parroter $parroter, Request $request)
    {
        $parroter->saveUserFromRequest($request);
        return view('success');
    }

    /**
     * @var array
     */
    protected $callbackIdRouting = [
        'all-the-parrots' => 'allTheParrots'
    ];

    /**
     * Slack Menu Options can only hit one URL for a given Slack app, so every menu option will hit this endpoint.
     * Use it to route the request to other controller methods based on the callback.
     *
     * @param Parroter $parroter
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function routeRequest(Parroter $parroter, Request $request)
    {
        $payload = json_decode($request->input('payload'));

        // send the user a message asking them to authenticate with the app
        if(!$parroter->userTokenExists($payload->user->id)) {
            return $this->requestAuthentication($parroter, $request);
        }

        // perform the correct callback
        if(array_key_exists($payload->callback_id, $this->callbackIdRouting)) {
            return $this->{$this->callbackIdRouting[$payload->callback_id]}($parroter, $request);
        }

        // callback not found
        return response()->json('', 404);
    }

    /**
     * @param Parroter $parroter
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function requestAuthentication(Parroter $parroter, Request $request)
    {
        $payload = json_decode($request->input('payload'));
        $parroter->requestAuthentication($payload->channel->id, $payload->user->id);
        return response()->json('', 200);
    }

    /**
     * @param Parroter $parroter
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    protected function allTheParrots(Parroter $parroter, Request $request)
    {
        $payload = json_decode($request->input('payload'));
        if(
            $payload->type !== 'message_action'
            || $payload->message->type === 'ephemeral'
        ) {
            return response()->json('', 200);
        }

        $parroter->postReactions(
            $parroter->getUserToken($payload->user->id),
            $payload->channel->id,
            $payload->message->ts
        );

        return response()->json('', 200);
    }
}
