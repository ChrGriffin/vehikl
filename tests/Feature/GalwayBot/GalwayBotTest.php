<?php

namespace Tests\Feature\GalwayBot;

use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use Tests\Traits\MocksGuzzleHistory;

class GalwayBotTest extends TestCase
{
    use MocksGuzzleHistory;

    public function setUp(): void
    {
        parent::setUp();
        $this->guzzleHistory = [];
        $this->mockGuzzleHistory($this->guzzleHistory);
    }

    public function testItPostsToTheSlackChannelWithTheBusinessDay(): void
    {
        Carbon::setTestNow('2020-01-24');

        $apiHolidays = $this->loadTextFixture('holidays_api_ontario_2020.json');
        $this->guzzleHandler->append((new Response(200))->withBody($this->createResponseBody($apiHolidays)));
        $this->guzzleHandler->append(new Response(200));

        Artisan::call('galway:business-day');

        $requestContents = $this->getGuzzleHistory()[1]['request']->getBody()->getContents();
        $this->assertContains('17th business day', $requestContents);
    }
}
