<?php

namespace Tests\Feature\Parroter;

use App\Services\Parroter\Parroter;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\Traits\MocksGuzzleHistory;

class ReactionPostingTest extends ParroterTestCase
{
    use MocksGuzzleHistory;

    /**
     * @var mixed[]
     */
    private $slackRequest;

    /**
     * @var string
     */
    private $token;

    /**
     * @var Parroter
     */
    private $parroter;

    /**
     * @var MockObject
     */
    private $joliCodeSlackClient;

    public function setUp(): void
    {
        parent::setUp();

        $this->guzzleHistory = [];
        $this->mockGuzzleHistory($this->guzzleHistory);

        $this->token = 'xoxp-' . str_random(16);
        $this->slackRequest = $this->loadJsonFixture('slack_all_the_parrots.json');
        $this->parroter = $this->app->make(Parroter::class);
        $this->parroter->saveUserToken(
            array_get(json_decode($this->slackRequest['payload'], true), 'user.id'),
            $this->token
        );
    }

    /**
     * @param string|null $index
     * @return mixed
     */
    private function getSlackRequestPayload(?string $index = null)
    {
        return $index === null
            ? json_decode($this->slackRequest['payload'], true)
            : array_get(json_decode($this->slackRequest['payload'], true), $index);
    }

    /**
     * @return void
     */
    public function testItReactsToAGivenMessage(): void
    {
        $this->guzzleHandler->append(new Response(200, [], json_encode(['emoji' => ['parrot' => 'parrot.png']])));
        $this->guzzleHandler->append(new Response(200));
        $this->post(route('parroter.routeSlackRequest'), $this->slackRequest);

        $reactionRequest = collect($this->getGuzzleHistory())
            ->filter(function (array $request) {
                return ends_with($request['request']->getUri()->getPath(), 'reactions.add');
            })
            ->first();

        $this->assertNotNull($reactionRequest);
        $this->assertEquals(
            [
                'channel'   => $this->getSlackRequestPayload('channel.id'),
                'timestamp' => $this->getSlackRequestPayload('message_ts'),
                'name'      => 'parrot'
            ],
            json_decode($reactionRequest['request']->getBody()->getContents(), true)
        );
    }

    /**
     * @return void
     */
    public function testItReactsWithOnlyParrotEmojis(): void
    {
        $parrots = [
            'superparrot'  => 'parrot.png',
            'hyperparrot'  => 'parrot.jpg',
            'geraltparrot' => 'parrot.gif',
            'rhino'        => 'notaparrot.png',
            'partyhippo'   => 'ahippo.jpeg'
        ];

        $this->guzzleHandler->append(new Response(200, [], json_encode(['emoji' => $parrots])));
        $this->guzzleHandler->append(new Response(200), new Response(200), new Response(200));
        $this->post(route('parroter.routeSlackRequest'), $this->slackRequest);

        $requests = collect($this->getGuzzleHistory())
            ->filter(function (array $request) {
                return ends_with($request['request']->getUri()->getPath(), 'reactions.add');
            });

        $this->assertEquals(3, $requests->count());
        $requests->each(function (array $request) {
            $this->assertContains('parrot', json_decode($request['request']->getBody()->getContents())->name);
        });
    }
}
