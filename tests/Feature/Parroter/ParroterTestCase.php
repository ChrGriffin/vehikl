<?php

namespace Tests\Feature\Parroter;

use App\SlackApp;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

abstract class ParroterTestCase extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->createSlackApp();
    }

    /**
     * @return SlackApp
     */
    protected function getSlackApp(): SlackApp
    {
        return SlackApp::where('name', 'parroter')->firstOrFail();
    }

    /**
     * @return void
     */
    private function createSlackApp(): void
    {
        SlackApp::updateOrCreate(
            ['name' => 'parroter'],
            ['data' => ['tokens' => []]]
        );
    }
}
