<?php

namespace Tests\Feature\Parroter;

use GuzzleHttp\Psr7\Response;
use Tests\Traits\MocksGuzzleHistory;

class RequestRoutingTest extends ParroterTestCase
{
    use MocksGuzzleHistory;

    /**
     * @return void
     */
    public function testItReturnsSuccessForAllTheParrotsRoute(): void
    {
        $this->guzzleHandler->append(new Response(200));
        $response = $this->post(
            route('parroter.routeSlackRequest'),
            $this->loadJsonFixture('slack_all_the_parrots.json')
        );

        $response->assertOk();
    }

    /**
     * @return void
     */
    public function testItReturnsNotFoundForAnUndefinedRoute(): void
    {
        $response = $this->post(
            'api/parroter/this-route-definitely-does-not-exist-trololol',
            $this->loadJsonFixture('slack_all_the_parrots.json')
        );

        $response->assertNotFound();
    }

    /**
     * @return void
     */
    public function testItRequestsAuthenticationIfTheUserIsNotAuthenticated(): void
    {
        $this->guzzleHandler->append(new Response(200));
        $this->post(
            route('parroter.routeSlackRequest'),
            $this->loadJsonFixture('slack_all_the_parrots.json')
        );

        $reactionPost = collect($this->getGuzzleHistory())
            ->filter(function (array $request) {
                return ends_with($request['request']->getUri()->getPath(), 'postEphemeral');
            })
            ->first();

        $this->assertNotNull($reactionPost);
        $this->assertContains(
            'You must authenticate with Parroter before you can use to post reactions.',
            $reactionPost['request']->getBody()->getContents()
        );
    }
}
