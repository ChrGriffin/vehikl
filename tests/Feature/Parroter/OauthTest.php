<?php

namespace Tests\Feature\Parroter;

use Exception;
use GuzzleHttp\Psr7\Response;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\Traits\MocksGuzzle;

class OauthTest extends ParroterTestCase
{
    use MocksGuzzle;

    /**
     * @return void
     */
    public function testItRedirectsToSlackAuthenticationToRequestIdentityScope(): void
    {
        $response = $this->get(route('parroter.oauth'));

        $response->assertStatus(302);
        $this->assertResponseRedirectsToSlackWithClientInfo($response, 'reaction');
        $this->assertRedirectRequestsScope('identity.basic', $response);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testItRedirectsToSlackAuthenticationToRequestReactionScope(): void
    {
        $this->mockSocialiteToReturnUserIdAndToken(random_int(100, 999), str_random(16));
        $this->mockSocialiteToAcceptSlackDriverFromWithMethod([
            'client_id' => config('services.slack.parroter.client_id'),
            'client_secret' => config('services.slack.parroter.client_secret'),
            'redirect' => config('services.slack.parroter.redirect_final')
        ]);

        $response = $this->get(route('parroter.oauth.callback.reaction'));

        $response->assertStatus(302);
        $this->assertResponseRedirectsToSlackWithClientInfo($response, 'final');
        $this->assertRedirectRequestsScope('reactions:write', $response);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testItSavesASuccessfullyAuthenticatedUserToTheDatabase(): void
    {
        $id = random_int(100, 999);
        $token = str_random(16);
        $this->guzzleHandler->append(new Response(200, [], json_encode(['user' => ['id' => $id]])));
        $this->mockSocialiteToReturnUserIdAndToken($id, $token);
        $this->mockSocialiteToAcceptSlackDriverFromWithMethod([
            'client_id' => config('services.slack.parroter.client_id'),
            'client_secret' => config('services.slack.parroter.client_secret'),
            'redirect' => config('services.slack.parroter.redirect_final')
        ]);

        $this->get(route('parroter.oauth.callback.final'));

        $tokens = $this->getSlackApp()->data['tokens'];
        $this->assertArrayHasKey($id, $tokens);
        $this->assertEquals($token, $tokens[$id]);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testItInformsSuccessIfTheUserIsSuccessfullyAuthenticated(): void
    {
        $this->guzzleHandler->append(
            new Response(200, [], json_encode(['user' => ['id' => random_int(100, 999)]]))
        );
        $this->mockSocialiteToReturnUserIdAndToken(random_int(100, 999), str_random(16));
        $this->mockSocialiteToAcceptSlackDriverFromWithMethod([
            'client_id' => config('services.slack.parroter.client_id'),
            'client_secret' => config('services.slack.parroter.client_secret'),
            'redirect' => config('services.slack.parroter.redirect_final')
        ]);

        $response = $this->get(route('parroter.oauth.callback.final'));

        $response->assertOk();
        $response->assertSeeText('Successfully authenticated!');
    }

    /**
     * @param TestResponse $response
     * @param null|string $redirectIndex
     * @return void
     */
    private function assertResponseRedirectsToSlackWithClientInfo(
        TestResponse $response,
        ?string $redirectIndex = null
    ): void
    {
        $this->assertRedirectUrlContains('https://slack.com/oauth/authorize', $response);
        $this->assertRedirectUrlContains('client_id=' . config('services.slack.parroter.client_id'), $response);
        $this->assertRedirectUrlContains(
            'redirect_uri=' . urlencode(
                config(str_finish(
                    'services.slack.parroter.redirect',
                    $redirectIndex !== null ? '_' . $redirectIndex : ''
                ))
            ),
            $response
        );
    }
}
