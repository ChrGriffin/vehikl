<?php

namespace Tests\Feature\DeCarloBot;

use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use Tests\Traits\MocksGuzzleHistory;

class DeCarloBotTest extends TestCase
{
    use MocksGuzzleHistory;

    public function setUp(): void
    {
        parent::setUp();
        $this->guzzleHistory = [];
        $this->mockGuzzleHistory($this->guzzleHistory);
    }

    public function testItPostsToTheSlackChannelWithARandomDaily(): void
    {
        $apiWord = $this->loadTextFixture('words_api_word_with_definition.json');
        $this->guzzleHandler->append((new Response(200))->withBody($this->createResponseBody($apiWord)));
        $this->guzzleHandler->append(new Response(200));

        Artisan::call('decarlo:daily', ['--daily' => 'Word']);

        $requestContents = $this->getGuzzleHistory()[1]['request']->getBody()->getContents();
        $this->assertContains(json_decode($apiWord)->word, $requestContents);
        $this->assertContains(json_decode($apiWord)->results[0]->definition, $requestContents);
    }
}
