<?php

namespace Tests\Feature\Mentorship;

use App\Entities\Mentorship\Skill;
use App\Entities\Mentorship\User;
use GuzzleHttp\Psr7\Response;

class ListMentorsTest extends MentorshipTestCase
{
    /**
     * @return void
     */
    public function testItReturnsSuccessFromAValidSlackRequest(): void
    {
        $this->guzzleHandler->append(new Response(200));
        $this->guzzleHandler->append(new Response(200));

        $response = $this->post(route('mentor.list'), $this->loadJsonFixture('slack_list_mentors.json'));
        $response->assertOk();
    }

    /**
     * @return void
     */
    public function testItSendsTheUserAListOfMentors(): void
    {
        $this->guzzleHandler->append(new Response(200));

        $mentors = $this->provideUsersWithSkills()->map(function (array $mentorInfo) {
           $mentor = User::create(array_only($mentorInfo, ['id', 'name']));
           $mentorInfo['skills']->each(function (string $skill) use ($mentor) {
               $mentor->skills()->attach(Skill::firstOrCreate(['name' => $skill]), ['type' => 'mentor']);
           });

           return $mentor->fresh();
        });

        $this->post(route('mentor.list'), $this->loadJsonFixture('slack_list_mentors.json'));

        $mentors->each(function (User $mentor) {
            $expectedString = '<@' . $mentor->name . '>'
                . ' is willing to provide mentoring in the following topics: *'
                . $mentor->skills->implode('name', ', ')
                . '*';

            $this->assertContains($expectedString, (string) $this->getGuzzleHistory()[0]['request']->getBody());
        });
    }
}
