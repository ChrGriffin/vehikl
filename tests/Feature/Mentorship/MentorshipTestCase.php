<?php

namespace Tests\Feature\Mentorship;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Traits\MocksGuzzleHistory;

abstract class MentorshipTestCase extends TestCase
{
    use RefreshDatabase, MocksGuzzleHistory;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->guzzleHistory = [];
        $this->mockGuzzleHistory($this->guzzleHistory);
        $this->withoutMiddleware(ThrottleRequests::class);
    }

    /**
     * @return Collection
     */
    protected function provideUsersWithSkills(): Collection
    {
        return collect([
            [
                'id'     => '111',
                'name'   => 'geralt',
                'skills' => collect(['swordsmanship', 'alchemy', 'riding', 'signs'])
            ],
            [
                'id'     => '2222',
                'name'   => 'yennefer',
                'skills' => collect(['sorcery', 'teleporation'])
            ],
            [
                'id'     => '33333',
                'name'   => 'triss',
                'skills' => collect(['sorcery', 'fire'])
            ],
            [
                'id'     => '44444444',
                'name'   => 'cirilla',
                'skills' => collect(['swordsmanship', 'translocation'])
            ],
        ]);
    }
}
