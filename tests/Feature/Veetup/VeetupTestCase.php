<?php

namespace Tests\Feature\Parroter;

use App\SlackApp;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

abstract class VeetupTestCase extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->createSlackApp();
    }

    /**
     * @return SlackApp
     */
    protected function getSlackApp(): SlackApp
    {
        return SlackApp::where('name', 'veetup')->firstOrFail();
    }

    /**
     * @return void
     */
    private function createSlackApp(): void
    {
        SlackApp::updateOrCreate(
            ['name' => 'veetup'],
            ['data' => ['groups' => []]]
        );
    }
}
