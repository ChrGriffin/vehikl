<?php

namespace Tests\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;

trait MocksGuzzle
{
    /**
     * @var MockHandler
     */
    private $guzzleHandler;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->mockGuzzle();
    }

    /**
     * @return void
     */
    protected function mockGuzzle(): void
    {
        $this->guzzleHandler = new MockHandler();
        $stack = HandlerStack::create($this->guzzleHandler);
        $mockedGuzzle = new Client(['handler' => $stack]);
        $this->app->bind('GuzzleHttp\Client', function () use ($mockedGuzzle) {
            return $mockedGuzzle;
        });
    }

    /**
     * @return void
     */
    protected function restoreGuzzle(): void
    {
        $this->app->bind('GuzzleHttp\Client', function () {
            return new Client;
        });
    }

    /**
     * @return void
     */
    public function tearDown(): void
    {
        $this->restoreGuzzle();
        parent::tearDown();
    }
}
