<?php

namespace Tests\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Stream;

trait MocksGuzzleHistory
{
    /** @var array */
    protected $guzzleHistory;

    /** @var MockHandler */
    protected $guzzleHandler;

    public function setUp(): void
    {
        parent::setUp();
        $this->guzzleHistory = [];
        $this->mockGuzzleHistory($this->guzzleHistory);
    }

    protected function mockGuzzleHistory(array &$container): array
    {
        $this->guzzleHandler = new MockHandler();
        $history = Middleware::history($container);
        $stack = HandlerStack::create($this->guzzleHandler);
        $stack->push($history);
        $mockedGuzzle = new Client(['handler' => $stack]);
        $this->app->bind('GuzzleHttp\Client', function () use ($mockedGuzzle) {
            return $mockedGuzzle;
        });

        return $container;
    }

    protected function getGuzzleHistory(): array
    {
        return $this->guzzleHistory;
    }

    protected function restoreGuzzle(): void
    {
        $this->app->bind('GuzzleHttp\Client', function () {
            return new Client;
        });
    }

    protected function createResponseBody(string $string)
    {
        $stream = fopen('php://memory','r+');
        fwrite($stream, $string);
        rewind($stream);

        return new Stream($stream);
    }

    public function tearDown(): void
    {
        $this->restoreGuzzle();
        parent::tearDown();
    }
}
