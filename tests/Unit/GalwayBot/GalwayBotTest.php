<?php

namespace Tests\Unit\GalwayBot;

use App\Services\BusinessDayChecker\BusinessDayChecker;
use App\Services\GalwayBot\GalwayBot;
use App\Services\HolidaysApi\Holiday;
use App\Services\HolidaysApi\HolidaysApi;
use App\Services\Slack\Slack;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;
use Tests\Traits\MocksGuzzleHistory;

class GalwayBotTest extends TestCase
{
    use MocksGuzzleHistory;

    public function setUp(): void
    {
        parent::setUp();
        $this->guzzleHistory = [];
        $this->mockGuzzleHistory($this->guzzleHistory);
        $this->mockHolidaysApi();
    }

    public function testItCanBeInstantiatedWithASlackClientAndABusinessDayChecker(): void
    {
        $galwayBot = new GalwayBot($this->app->get(Slack::class), $this->app->get(BusinessDayChecker::class));
        $this->assertInstanceOf(GalwayBot::class, $galwayBot);
    }

    public function testItCanBePulledFromTheApplicationContainer(): void
    {
        $this->assertInstanceOf(GalwayBot::class, $this->app->get(GalwayBot::class));
    }

    public function testItPostsTheCurrentBusinessDay(): void
    {
        Carbon::setTestNow('2020-01-02');
        $this->guzzleHandler->append(new Response(200));

        /** @var GalwayBot $galwayBot */
        $galwayBot = $this->app->get(GalwayBot::class);
        $galwayBot->postIfTodayIsBusinessDay();

        $request = json_decode($this->getGuzzleHistory()[0]['request']->getBody()->getContents());
        $this->assertContains('1st business day', $request->text);
    }

    private function mockHolidaysApi(): void
    {
        $holidays = collect($this->loadJsonFixture('holidays_api_ontario_2020.json', false)->province->holidays)
            ->map(function (\stdClass $holiday) {
                return new Holiday($holiday);
            });

        $fakeHolidaysApi = $this->createMock(HolidaysApi::class);
        $fakeHolidaysApi->method('holidays')->willReturn($holidays);

        $this->app->bind(HolidaysApi::class, function () use ($fakeHolidaysApi) {
            return $fakeHolidaysApi;
        });
    }
}
