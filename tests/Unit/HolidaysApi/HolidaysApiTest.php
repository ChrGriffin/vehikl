<?php

namespace Tests\Unit\WordsApi;

use App\Services\HolidaysApi\Holiday;
use App\Services\HolidaysApi\HolidaysApi;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Traits\MocksGuzzleHistory;

class HolidaysApiTest extends TestCase
{
    use MocksGuzzleHistory;

    public function setUp(): void
    {
        parent::setUp();
        $this->guzzleHistory = [];
        $this->mockGuzzleHistory($this->guzzleHistory);
    }

    public function testItCanBeInstantiatedWithAGuzzleClient(): void
    {
        $this->assertInstanceOf(HolidaysApi::class, new HolidaysApi(new Client));
    }

    public function testItCanBePulledFromTheApplicationContainer(): void
    {
        $this->assertInstanceOf(HolidaysApi::class, $this->app->get(HolidaysApi::class));
    }

    public function testHolidaysReturnsACollectionOfHolidays(): void
    {
        $data = $this->loadTextFixture('holidays_api_ontario_2020.json');
        $this->guzzleHandler->append((new Response(200))->withBody($this->createResponseBody($data)));

        /** @var Collection $holidays */
        $holidays = $this->app->get(HolidaysApi::class)->holidays();

        $this->assertInstanceOf(Collection::class, $holidays);
        $this->assertGreaterThan(0, $holidays->count());
        $holidays->each(function (Holiday $holiday) {
            $this->assertInstanceOf(Holiday::class, $holiday);
        });
    }
}
