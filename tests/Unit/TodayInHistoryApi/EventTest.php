<?php

namespace Tests\Unit\TodayInHistoryApi;

use App\Services\TodayInHistoryApi\Event;
use Tests\TestCase;

class EventTest extends TestCase
{
    public function testItLoadsValuesFromAValidWordsApiResponse(): void
    {
        $validData = $this->loadJsonFixture('history_api_today.json', false)->data->Events[0];
        $event = new Event($validData);

        $this->assertEquals($validData->text, $event->getText());
        $this->assertEquals($validData->year, $event->getYear());
    }

    public function testItCanTransformToAnArrayWithAllAttributes(): void
    {
        $validData = $this->loadJsonFixture('history_api_today.json', false)->data->Events[0];
        $event = new Event($validData);

        $this->assertTrue(is_array($event->toArray()));
        $this->assertArrayHasKeys(['text', 'year'], $event->toArray());
    }
}
