<?php

namespace Tests\Unit\WordsApi;

use App\Services\TodayInHistoryApi\Event;
use App\Services\TodayInHistoryApi\TodayInHistoryApi;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;
use Tests\Traits\MocksGuzzleHistory;

class TodayInHistoryApiTest extends TestCase
{
    use MocksGuzzleHistory;

    public function setUp(): void
    {
        parent::setUp();
        $this->guzzleHistory = [];
        $this->mockGuzzleHistory($this->guzzleHistory);
    }

    public function testItCanBeInstantiatedWithAGuzzleClient(): void
    {
        $this->assertInstanceOf(TodayInHistoryApi::class, new TodayInHistoryApi(new Client));
    }

    public function testItCanBePulledFromTheApplicationContainer(): void
    {
        $this->assertInstanceOf(TodayInHistoryApi::class, $this->app->get(TodayInHistoryApi::class));
    }

    public function testRandomReturnsARandomEvent(): void
    {
        $data = $this->loadTextFixture('history_api_today.json');
        $this->guzzleHandler->append((new Response(200))->withBody($this->createResponseBody($data)));

        /** @var Event $event */
        $event = $this->app->get(TodayInHistoryApi::class)->random();

        $this->assertInstanceOf(Event::class, $event);
    }
}
