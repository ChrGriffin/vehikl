<?php

namespace Tests\Unit\WordsApi;

use App\Services\WordsApi\Word;
use Tests\TestCase;

class WordTest extends TestCase
{
    public function testItLoadsValuesFromAValidWordsApiResponse(): void
    {
        $validData = json_decode(json_encode($this->loadJsonFixture('words_api_word_with_definition.json')));
        $word = new Word($validData);

        $this->assertEquals($validData->word, $word->getWord());
        $this->assertEquals($validData->results[0]->definition, $word->getDefinition());
        $this->assertEquals($validData->results[0]->partOfSpeech, $word->getType());
        $this->assertEquals($validData->pronunciation->all, $word->getPronunciation());
    }

    public function testItSetsOptionalFieldsToNullIfNotPresentInPayload(): void
    {
        $validData = json_decode(json_encode($this->loadJsonFixture('words_api_word_without_definition.json')));
        $word = new Word($validData);

        $this->assertNull($word->getDefinition());
        $this->assertNull($word->getType());
    }

    public function testItCanTransformToAnArrayWithAllAttributes(): void
    {
        $validData = json_decode(json_encode($this->loadJsonFixture('words_api_word_with_definition.json')));
        $word = new Word($validData);

        $this->assertTrue(is_array($word->toArray()));
        $this->assertArrayHasKeys(['word', 'definition', 'type', 'pronunciation'], $word->toArray());
    }
}
