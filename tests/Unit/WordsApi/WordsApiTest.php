<?php

namespace Tests\Unit\WordsApi;

use App\Services\WordsApi\Word;
use App\Services\WordsApi\WordsApi;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;
use Tests\Traits\MocksGuzzleHistory;

class WordsApiTest extends TestCase
{
    use MocksGuzzleHistory;

    public function setUp(): void
    {
        parent::setUp();
        $this->guzzleHistory = [];
        $this->mockGuzzleHistory($this->guzzleHistory);
    }

    public function testItCanBeInstantiatedWithAGuzzleClient(): void
    {
        $this->assertInstanceOf(WordsApi::class, new WordsApi(new Client));
    }

    public function testItCanBePulledFromTheApplicationContainer(): void
    {
        $this->assertInstanceOf(WordsApi::class, $this->app->get(WordsApi::class));
    }

    public function testRandomReturnsARandomWord(): void
    {
        $data = $this->loadTextFixture('words_api_word_without_definition.json');
        $this->guzzleHandler->append((new Response(200))->withBody($this->createResponseBody($data)));

        /** @var Word $word */
        $word = $this->app->get(WordsApi::class)->random();

        $this->assertInstanceOf(Word::class, $word);
        $this->assertEquals(json_decode($data)->word, $word->getWord());
    }

    public function testRandomReturnsARandomWordWithNoSpaces(): void
    {
        $withSpaces = $this->loadTextFixture('words_api_word_with_spaces.json');
        $withoutSpaces = $this->loadTextFixture('words_api_word_without_definition.json');
        $this->guzzleHandler->append((new Response(200))->withBody($this->createResponseBody($withSpaces)));
        $this->guzzleHandler->append((new Response(200))->withBody($this->createResponseBody($withSpaces)));
        $this->guzzleHandler->append((new Response(200))->withBody($this->createResponseBody($withoutSpaces)));

        /** @var Word $word */
        $word = $this->app->get(WordsApi::class)->random();

        $this->assertInstanceOf(Word::class, $word);
        $this->assertEquals(json_decode($withoutSpaces)->word, $word->getWord());
    }

    public function testRandomWithDefinitionReturnsARandomWordWithADefinition(): void
    {
        $withSpaces = $this->loadTextFixture('words_api_word_with_spaces.json');
        $withoutDefinition = $this->loadTextFixture('words_api_word_without_definition.json');
        $withDefinition = $this->loadTextFixture('words_api_word_with_definition.json');

        $this->guzzleHandler->append((new Response(200))->withBody($this->createResponseBody($withoutDefinition)));
        $this->guzzleHandler->append((new Response(200))->withBody($this->createResponseBody($withSpaces)));
        $this->guzzleHandler->append((new Response(200))->withBody($this->createResponseBody($withoutDefinition)));
        $this->guzzleHandler->append((new Response(200))->withBody($this->createResponseBody($withDefinition)));

        /** @var Word $word */
        $word = $this->app->get(WordsApi::class)->randomWithDefinition();

        $this->assertInstanceOf(Word::class, $word);
        $this->assertEquals(json_decode($withDefinition)->word, $word->getWord());
        $this->assertNotNull($word->getDefinition());
    }
}
