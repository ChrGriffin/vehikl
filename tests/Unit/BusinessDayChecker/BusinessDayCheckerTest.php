<?php

namespace Tests\Unit\BusinessDayChecker;

use App\Services\BusinessDayChecker\BusinessDayChecker;
use App\Services\BusinessDayChecker\NotABusinessDayException;
use App\Services\HolidaysApi\Holiday;
use App\Services\HolidaysApi\HolidaysApi;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Tests\TestCase;

class BusinessDayCheckerTest extends TestCase
{
    /** @const string  */
    const DATE_2020 = '2020-06-19';

    public function setUp()
    {
        parent::setUp();
        Carbon::setTestNow(self::DATE_2020);
        $this->fakeHolidaysApi(collect());
    }

    public function testItCanBeInstantiatedWithAHolidaysApi(): void
    {
        $this->assertInstanceOf(BusinessDayChecker::class, new BusinessDayChecker($this->app->get(HolidaysApi::class)));
    }

    public function testItCanBePulledFromTheApplicationContainer(): void
    {
        $this->assertInstanceOf(BusinessDayChecker::class, $this->app->get(BusinessDayChecker::class));
    }

    public function testItDoesNotCountAWeekendDayAsABusinessDays(): void
    {
        /** @var BusinessDayChecker $checker */
        $checker = $this->app->get(BusinessDayChecker::class);
        $this->assertFalse($checker->dayIsBusinessDay(Carbon::parse('2020-01-04')));
    }

    public function testItDoesNotCountAHolidayAsABusinessDays(): void
    {
        $this->fakeHolidaysApi(collect([
            new Holiday((object) [
                'nameEn' => 'Geralt of Rivia Day',
                'date' => '2020-01-06'
            ])
        ]));

        /** @var BusinessDayChecker $checker */
        $checker = $this->app->get(BusinessDayChecker::class);

        $this->assertFalse($checker->dayIsBusinessDay(Carbon::parse('2020-01-06')));
    }

    public function provideBusinessDays(): array
    {
        return [
            '2020-01-07' => ['date' => Carbon::parse('2020-01-07'), 'businessDayNumber' => 4],
            '2020-01-23' => ['date' => Carbon::parse('2020-01-23'), 'businessDayNumber' => 16],
            '2020-11-20' => ['date' => Carbon::parse('2020-11-20'), 'businessDayNumber' => 226]
        ];
    }

    /** @dataProvider provideBusinessDays */
    public function testItCountsANormalWeekdayAsABusinessDay(Carbon $date): void
    {
        /** @var BusinessDayChecker $checker */
        $checker = $this->app->get(BusinessDayChecker::class);
        $this->assertTrue($checker->dayIsBusinessDay($date));
    }

    /** @dataProvider provideBusinessDays */
    public function testItGetsTheCurrentBusinessDay(Carbon $date, int $businessDayNumber): void
    {
        $holidays = collect($this->loadJsonFixture('holidays_api_ontario_2020.json', false)->province->holidays)
            ->map(function (\stdClass $holiday) {
                return new Holiday($holiday);
            });
        $this->fakeHolidaysApi($holidays);

        /** @var BusinessDayChecker $checker */
        $checker = $this->app->get(BusinessDayChecker::class);
        $this->assertEquals($businessDayNumber, $checker->numberForBusinessDay($date));
    }

    public function testItThrowsAnExceptionIfTheDayToGetANumberForIsNotABusinessDay()
    {
        $this->expectException(NotABusinessDayException::class);

        /** @var BusinessDayChecker $checker */
        $checker = $this->app->get(BusinessDayChecker::class);
        $checker->numberForBusinessDay(Carbon::parse('2020-01-04'));
    }

    private function fakeHolidaysApi(Collection $return): void
    {
        $fakeHolidaysApi = $this->createMock(HolidaysApi::class);
        $fakeHolidaysApi->method('holidays')->willReturn($return);
        $this->app->bind(HolidaysApi::class, function () use ($fakeHolidaysApi) {
            return $fakeHolidaysApi;
        });
    }
}
