<?php

namespace Tests\Unit\WordsApi;

use App\Services\QuotesApi\Quote;
use Tests\TestCase;

class QuoteTest extends TestCase
{
    public function testItLoadsValuesFromAValidQuotesApiResponse(): void
    {
        $validData = $this->loadJsonFixture('quotes_api_quote.json', false)[0];
        $quote = new Quote($validData);

        $this->assertEquals($validData->quote, $quote->getQuote());
        $this->assertEquals($validData->author, $quote->getAuthor());
    }

    public function testItCanTransformToAnArrayWithAllAttributes(): void
    {
        $validData = $this->loadJsonFixture('quotes_api_quote.json', false)[0];
        $quote = new Quote($validData);

        $this->assertTrue(is_array($quote->toArray()));
        $this->assertArrayHasKeys(['quote', 'author'], $quote->toArray());
    }
}
