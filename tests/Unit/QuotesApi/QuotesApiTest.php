<?php

namespace Tests\Unit\WordsApi;

use App\Services\QuotesApi\Quote;
use App\Services\QuotesApi\QuotesApi;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;
use Tests\Traits\MocksGuzzleHistory;

class QuotesApiTest extends TestCase
{
    use MocksGuzzleHistory;

    public function setUp(): void
    {
        parent::setUp();
        $this->guzzleHistory = [];
        $this->mockGuzzleHistory($this->guzzleHistory);
    }

    public function testItCanBeInstantiatedWithAGuzzleClient(): void
    {
        $this->assertInstanceOf(QuotesApi::class, new QuotesApi(new Client));
    }

    public function testItCanBePulledFromTheApplicationContainer(): void
    {
        $this->assertInstanceOf(QuotesApi::class, $this->app->get(QuotesApi::class));
    }

    public function testRandomReturnsARandomQuote(): void
    {
        $data = $this->loadTextFixture('quotes_api_quote.json');
        $this->guzzleHandler->append((new Response(200))->withBody($this->createResponseBody($data)));

        /** @var Quote $quote */
        $quote = $this->app->get(QuotesApi::class)->random();

        $this->assertInstanceOf(Quote::class, $quote);
        $this->assertEquals(json_decode($data)[0]->quote, $quote->getQuote());
    }
}
