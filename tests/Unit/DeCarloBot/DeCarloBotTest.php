<?php

namespace Tests\Unit\DeCarloBot;

use App\Services\DeCarloBot\Dailies\Quote;
use App\Services\DeCarloBot\Dailies\Word;
use App\Services\DeCarloBot\DeCarloBot;
use App\Services\QuotesApi\QuotesApi;
use App\Services\Slack\Slack;
use App\Services\WordsApi\WordsApi;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;
use Tests\Traits\MocksGuzzleHistory;

class DeCarloBotTest extends TestCase
{
    use MocksGuzzleHistory;

    public function testItCanBeInstantiatedWithASlackClient(): void
    {
        $deCarloBot = new DeCarloBot($this->app->get(Slack::class));
        $this->assertInstanceOf(DeCarloBot::class, $deCarloBot);
    }

    public function testItCanBePulledFromTheApplicationContainer(): void
    {
        $this->assertInstanceOf(DeCarloBot::class, $this->app->get(DeCarloBot::class));
    }

    public function testItPostsToSlackWithTheWordOfTheDay(): void
    {
        $this->guzzleHandler->append(new Response(200));
        $apiWord = json_decode($this->loadTextFixture('words_api_word_with_definition.json'));

        $fakeWordsApi = $this->createMock(WordsApi::class);
        $fakeWordsApi->method('randomWithDefinition')->willReturn(new \App\Services\WordsApi\Word($apiWord));
        $this->app->bind(WordsApi::class, function () use ($fakeWordsApi) {
            return $fakeWordsApi;
        });

        /** @var DeCarloBot $deCarloBot */
        $deCarloBot = $this->app->get(DeCarloBot::class);
        $deCarloBot->postDaily(Word::class);

        $requestContents = $this->getGuzzleHistory()[0]['request']->getBody()->getContents();
        $this->assertContains($apiWord->word, $requestContents);
        $this->assertContains($apiWord->results[0]->definition, $requestContents);
    }

    public function testItPostsToSlackWithTheQuoteOfTheDay(): void
    {
        $this->guzzleHandler->append(new Response(200));
        $apiQuote = $this->loadJsonFixture('quotes_api_quote.json', false)[0];

        $fakeQuotesApi = $this->createMock(QuotesApi::class);
        $fakeQuotesApi->method('random')->willReturn(new \App\Services\QuotesApi\Quote($apiQuote));
        $this->app->bind(QuotesApi::class, function () use ($fakeQuotesApi) {
            return $fakeQuotesApi;
        });

        /** @var DeCarloBot $deCarloBot */
        $deCarloBot = $this->app->get(DeCarloBot::class);
        $deCarloBot->postDaily(Quote::class);

        $requestContents = $this->getGuzzleHistory()[0]['request']->getBody()->getContents();
        $this->assertContains($apiQuote->quote, $requestContents);
        $this->assertContains($apiQuote->author, $requestContents);
    }
}
