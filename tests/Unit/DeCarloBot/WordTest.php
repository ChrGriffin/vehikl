<?php

namespace Tests\Unit\DeCarloBot;

use App\Services\DeCarloBot\Dailies\Word;
use App\Services\DeCarloBot\Exceptions\DailyNotGeneratedException;
use App\Services\WordsApi\WordsApi;
use App\Services\WordsApi\Word as ApiWord;
use Tests\TestCase;

class WordTest extends TestCase
{
    public function testItCanBeInstantiatedWithAWordsApi(): void
    {
        $this->assertInstanceOf(Word::class, new Word($this->app->get(WordsApi::class)));
    }

    public function testItCanBePulledFromTheApplicationContainer(): void
    {
        $this->assertInstanceOf(Word::class, $this->app->get(Word::class));
    }

    public function testItGetsARandomWord(): void
    {
        $this->mockWordsApi((object) ['word' => 'correct horse battery staple']);
        /** @var Word $word */
        $word = $this->app->get(Word::class);

        $word->generate();

        $this->assertContains('correct horse battery staple', $word->toSlackMessage());
    }

    public function testItThrowsAnExceptionIfNotGeneratedBeforeAccessingArray(): void
    {
        $this->expectException(DailyNotGeneratedException::class);

        $this->mockWordsApi();
        /** @var Word $word */
        $word = $this->app->get(Word::class);

        $word->toSlackMessage();
    }

    public function provideNullParameterCombinations(): array
    {
        return [
            'Word only' => [
                'indexesToUnset' => ['results', 'syllables', 'pronunciation'],
                'expectedMessage' => 'Today\'s word of the day is *{{word}}*.'
            ],
            'Word and Definition' => [
                'indexesToUnset' => ['results.0.partOfSpeech', 'results.0.typeOf', 'syllables', 'pronunciation'],
                'expectedMessage' => 'Today\'s word of the day is *{{word}}*. It is defined as *{{definition}}*.'
            ],
            'Word and type' => [
                'indexesToUnset' => ['results.0.definition', 'results.0.typeOf', 'syllables', 'pronunciation'],
                'expectedMessage' => 'Today\'s word of the day is *{{word}}*. It is a {{type}}.'
            ],
            'Word and pronunciation' => [
                'indexesToUnset' => ['results', 'syllables'],
                'expectedMessage' => 'Today\'s word of the day is *{{word}}*. It is pronounced _{{pronunciation}}_.'
            ],
            'Word and Definition and Type' => [
                'indexesToUnset' => ['syllables', 'pronunciation'],
                'expectedMessage' => 'Today\'s word of the day is *{{word}}*. It is a {{type}} that is defined as *{{definition}}*.'
            ]
        ];
    }

    /** @dataProvider provideNullParameterCombinations */
    public function testItAdaptsTheMessageToWhichVariablesAreSet(array $indexesToUnset, string $expectedMessage): void
    {
        $validData = $this->loadJsonFixture('words_api_word_with_definition.json');
        $expectedMessage = str_replace('{{word}}', $validData['word'], $expectedMessage);
        $expectedMessage = str_replace('{{definition}}', $validData['results'][0]['definition'], $expectedMessage);
        $expectedMessage = str_replace('{{type}}', $validData['results'][0]['partOfSpeech'], $expectedMessage);
        $expectedMessage = str_replace('{{pronunciation}}', $validData['pronunciation']['all'], $expectedMessage);
        array_forget($validData, $indexesToUnset);
        $this->mockWordsApi(json_decode(json_encode($validData)));

        /** @var Word $word */
        $word = $this->app->get(Word::class);
        $word->generate();

        $this->assertEquals($expectedMessage, $word->toSlackMessage());
    }

    private function mockWordsApi(?\stdClass $apiWordData = null)
    {
        if(is_null($apiWordData)) {
            $apiWordData = $this->loadJsonFixture('words_api_word_with_definition.json', false);
        }

        $fakeWordApi = $this->createMock(WordsApi::class);
        $fakeWordApi->method('randomWithDefinition')->willReturn(new ApiWord($apiWordData));

        $this->app->bind(WordsApi::class, function () use ($fakeWordApi) {
            return $fakeWordApi;
        });
    }
}
