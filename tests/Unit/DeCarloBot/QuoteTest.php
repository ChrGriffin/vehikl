<?php

namespace Tests\Unit\DeCarloBot;

use App\Services\DeCarloBot\Dailies\Quote;
use App\Services\DeCarloBot\Exceptions\DailyNotGeneratedException;
use App\Services\QuotesApi\Quote as ApiQuote;
use App\Services\QuotesApi\QuotesApi;
use Tests\TestCase;

class QuoteTest extends TestCase
{
    public function testItCanBeInstantiatedWithAQuotesApi(): void
    {
        $this->assertInstanceOf(Quote::class, new Quote($this->app->get(QuotesApi::class)));
    }

    public function testItCanBePulledFromTheApplicationContainer(): void
    {
        $this->assertInstanceOf(Quote::class, $this->app->get(Quote::class));
    }

    public function testItGetsARandomQuote(): void
    {
        $this->mockQuotesApi((object) ['quote' => 'correct horse battery staple', 'author' => 'xkcd']);
        /** @var Quote $quote */
        $quote = $this->app->get(Quote::class);

        $quote->generate();

        $this->assertContains('correct horse battery staple', $quote->toSlackMessage());
        $this->assertContains('xkcd', $quote->toSlackMessage());
    }

    public function testItThrowsAnExceptionIfNotGeneratedBeforeAccessingArray(): void
    {
        $this->expectException(DailyNotGeneratedException::class);

        $this->mockQuotesApi();
        /** @var Quote $quote */
        $quote = $this->app->get(Quote::class);

        $quote->toSlackMessage();
    }

    private function mockQuotesApi(?\stdClass $apiQuoteData = null)
    {
        if(is_null($apiQuoteData)) {
            $apiQuoteData = $this->loadJsonFixture('quotes_api_quote.json', false)[0];
        }

        $fakeQuotesApi = $this->createMock(QuotesApi::class);
        $fakeQuotesApi->method('random')->willReturn(new ApiQuote($apiQuoteData));

        $this->app->bind(QuotesApi::class, function () use ($fakeQuotesApi) {
            return $fakeQuotesApi;
        });
    }
}
