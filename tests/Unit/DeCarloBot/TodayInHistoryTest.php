<?php

namespace Tests\Unit\DeCarloBot;

use App\Services\DeCarloBot\Dailies\TodayInHistory;
use App\Services\DeCarloBot\Exceptions\DailyNotGeneratedException;
use App\Services\TodayInHistoryApi\Event;
use App\Services\TodayInHistoryApi\TodayInHistoryApi;
use Tests\TestCase;

class TodayInHistoryTest extends TestCase
{
    public function testItCanBeInstantiatedWithATodayInHistoryApi(): void
    {
        $this->assertInstanceOf(TodayInHistory::class, new TodayInHistory($this->app->get(TodayInHistoryApi::class)));
    }

    public function testItCanBePulledFromTheApplicationContainer(): void
    {
        $this->assertInstanceOf(TodayInHistory::class, $this->app->get(TodayInHistory::class));
    }

    public function testItGetsARandomEvent(): void
    {
        $this->mockTodayInHistoryApi((object) [
            'text' => 'the greatest software developer in history was born',
            'year' => '1992'
        ]);

        /** @var TodayInHistory $todayInHistory */
        $todayInHistory = $this->app->get(TodayInHistory::class);

        $todayInHistory->generate();

        $this->assertContains('the greatest software developer in history was born', $todayInHistory->toSlackMessage());
        $this->assertContains('1992', $todayInHistory->toSlackMessage());
    }

    public function testItThrowsAnExceptionIfNotGeneratedBeforeAccessingArray(): void
    {
        $this->expectException(DailyNotGeneratedException::class);

        $this->mockTodayInHistoryApi();
        /** @var TodayInHistory $quote */
        $quote = $this->app->get(TodayInHistory::class);

        $quote->toSlackMessage();
    }

    private function mockTodayInHistoryApi(?\stdClass $apiTodayInHistoryData = null)
    {
        if(is_null($apiTodayInHistoryData)) {
            $apiTodayInHistoryData = $this->loadJsonFixture('history_api_today.json', false)->data->Events[0];
        }

        $fakeTodayInHistoryApi = $this->createMock(TodayInHistoryApi::class);
        $fakeTodayInHistoryApi->method('random')->willReturn(new Event($apiTodayInHistoryData));

        $this->app->bind(TodayInHistoryApi::class, function () use ($fakeTodayInHistoryApi) {
            return $fakeTodayInHistoryApi;
        });
    }
}
