<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\SocialiteManager;
use Laravel\Socialite\Two\User as Oauth2User;
use Mpociot\Socialite\Slack\Provider as Oauth2Provider;
use Mpociot\Socialite\Slack\Provider as SlackProvider;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function assertRedirectUrlContains(string $needle, TestResponse $response): void
    {
        $this->assertContains(
            $needle,
            $response->headers->get('location'),
            "Failed asserting that the response's redirect URL includes $needle."
        );
    }

    protected function assertRedirectRequestsScope(string $scope, TestResponse $response): void
    {
        $this->assertRedirectUrlContains('scope=' . urlencode($scope), $response);
    }

    public function assertArrayHasKeys(array $keys, array $array): void
    {
        foreach($keys as $key) {
            $this->assertArrayHasKey($key, $array);
        }
    }

    protected function mockSocialiteToReturnUserIdAndToken(int $id, string $token): void
    {
        $user = $this->createMock(Oauth2User::class);
        $user->method('getId')->willReturn($id);

        $driver = $this->createMock(Oauth2Provider::class);
        $driver->method('stateless')->willReturn($driver);
        $driver->method('getAccessToken')->willReturn($token);
        $driver->method('userFromToken')->willReturn($user);

        Socialite::shouldReceive('driver')->andReturn($driver);
    }

    protected function mockSocialiteToAcceptSlackDriverFromWithMethod(array $config): void
    {
        /** @var SocialiteManager $mockedSocialite */
        $socialite = app()->makeWith(SocialiteManager::class, ['app' => app()]);

        $socialite->extend('slack', function () use ($socialite, $config) {
            $provider = $socialite->buildProvider(SlackProvider::class, $config);
            return $provider->stateless();
        });

        Socialite::shouldReceive('with')->andReturn($socialite->with('slack'));
    }

    /** @return array|\stdClass */
    protected function loadJsonFixture(string $filename, bool $assoc = true)
    {
        return json_decode($this->loadTextFixture($filename), $assoc);
    }

    protected function loadTextFixture(string $filename): string
    {
        return file_get_contents(__DIR__ . '/fixtures/' . $filename);
    }
}
